public class CuentaDeAhorros {
    private static double tasaInteresAnual;
    private double saldoAhorros; 

    //Constructor
    CuentaDeAhorros(){       
    }
    
    //Constructor dandole un valor
    CuentaDeAhorros( double ahorros ){
        saldoAhorros = ahorros;
    }

    //Se calcula el interes mensula  de la cuenta la formula dada en el ejercicio
    public double calcularInteresMensual()
    {   double interesMensual = ( saldoAhorros * tasaInteresAnual / 12)/100;   
        return interesMensual;
    }


    public static void setTasaInteres(double tasaInteresAnual1){
        tasaInteresAnual = tasaInteresAnual1; } 

        public static void main(String[] args) {
            double saldoAhorrador1,saldoAhorrador2,interesMensual;
            CuentaDeAhorros ahorrador1 = new CuentaDeAhorros(2000);
            CuentaDeAhorros ahorrador2 = new CuentaDeAhorros(3000);

            CuentaDeAhorros.setTasaInteres(4); 
            CuentaDeAhorros.setTasaInteres(5);
            saldoAhorrador1=ahorrador1.saldoAhorros;
            interesMensual=ahorrador1.calcularInteresMensual();
            System.out.println("El interes generado por ahorrador1 en el primer mes "+
                            "\ncon la tasa anual del 4% ha sido : \n"+interesMensual);
            System.out.println("Ahora su saldo es : \n" + (saldoAhorrador1+interesMensual));           
            saldoAhorrador1=saldoAhorrador1+interesMensual;
            
            interesMensual=ahorrador1.calcularInteresMensual();
            System.out.println("El interes generado por ahorrador1 en el segundo mes " + 
                                "\ncon la tasa anual del 5% ha sido : \n"+interesMensual);
            System.out.println("Ahora su saldo es : \n" + (saldoAhorrador1+interesMensual));

            System.out.println("\n=========0=========\n");
            
            saldoAhorrador2=ahorrador2.saldoAhorros;
            interesMensual=ahorrador2.calcularInteresMensual();
            System.out.println("El interes generado por ahorrador2 en el primer mes " + 
                            "\ncon la tasa anual del 4% ha sido : \n"+interesMensual);
            System.out.println("Ahora su saldo es : \n" + (saldoAhorrador2+interesMensual));           
            saldoAhorrador2=saldoAhorrador2+interesMensual;

            interesMensual=ahorrador2.calcularInteresMensual();
            System.out.println("El interes generado por ahorrador2 en el segundo mes " + 
                            " \ncon la tasa anual del 5% ha sido : \n"+interesMensual);
            System.out.println("Ahora su saldo es : \n" + (saldoAhorrador2+interesMensual));
    }
}

